package org.custom.broker.saml.mappers;

import org.keycloak.broker.provider.AbstractIdentityProviderMapper;
import org.keycloak.broker.provider.BrokeredIdentityContext;
import org.keycloak.broker.provider.IdentityProviderMapper;
import org.keycloak.broker.saml.SAMLEndpoint;
import org.keycloak.broker.saml.SAMLIdentityProviderFactory;
import org.keycloak.dom.saml.v2.assertion.AssertionType;
import org.keycloak.dom.saml.v2.assertion.AttributeStatementType;
import org.keycloak.dom.saml.v2.assertion.AttributeType;
import org.keycloak.models.*;
import org.keycloak.models.utils.KeycloakModelUtils;
import org.keycloak.provider.ProviderConfigProperty;
import java.util.*;
import org.jboss.logging.Logger;


public class AttributeToGroupMapper extends AbstractIdentityProviderMapper implements IdentityProviderMapper {
    private static final String[] COMPATIBLE_PROVIDERS = {SAMLIdentityProviderFactory.PROVIDER_ID};
    private static final HashSet<IdentityProviderSyncMode> SUPPORTED_PROVIDER_MAPPER_SYNC_MODES = new HashSet<>(Arrays.asList(
            IdentityProviderSyncMode.IMPORT,
            IdentityProviderSyncMode.FORCE
    ));

    private static final List<ProviderConfigProperty> configProperties = new ArrayList<ProviderConfigProperty>();

    private static final String ATTRIBUTE_NAME = "attribute.name";
    private static final String ATTRIBUTE_FRIENDLY_NAME = "attribute.friendly.name";
    private static final String GROUP_PREFIX = "group.prefix";

    static {
        ProviderConfigProperty property;
        property = new ProviderConfigProperty();
        property.setName(ATTRIBUTE_NAME);
        property.setLabel("Attribute Name");
        property.setHelpText("Name of attribute to search for in assertion.  You can leave this blank and specify a friendly name instead.");
        property.setType(ProviderConfigProperty.STRING_TYPE);
        configProperties.add(property);

        property = new ProviderConfigProperty();
        property.setName(ATTRIBUTE_FRIENDLY_NAME);
        property.setLabel("Friendly Name");
        property.setHelpText("Friendly name of attribute to search for in assertion.  You can leave this blank and specify a name instead.");
        property.setType(ProviderConfigProperty.STRING_TYPE);
        configProperties.add(property);

        property = new ProviderConfigProperty();
        property.setName(GROUP_PREFIX);
        property.setLabel("Group prefix");
        property.setHelpText("Group prefix that will act as a filter what user groups will be imported.");
        property.setType(ProviderConfigProperty.STRING_TYPE);
        property.setDefaultValue("DSTAT_");
        configProperties.add(property);
    }

    public static final String PROVIDER_ID = "saml-group-idp-mapper";

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        return configProperties;
    }

    @Override
    public String getId() {
        return PROVIDER_ID;
    }

    @Override
    public String[] getCompatibleProviders() {
        return COMPATIBLE_PROVIDERS;
    }

    @Override
    public boolean supportsSyncMode(IdentityProviderSyncMode syncMode) { return SUPPORTED_PROVIDER_MAPPER_SYNC_MODES.contains(syncMode); }

    @Override
    public String getDisplayCategory() {
        return "Group Mapper";
    }

    @Override
    public String getDisplayType() {
        return "SAML Attribute to Group";
    }

    @Override
    public void importNewUser(KeycloakSession session, RealmModel realm, UserModel user, IdentityProviderMapperModel mapperModel, BrokeredIdentityContext context) {
        AddUserToGroup(session, realm, user, mapperModel, context, true);
    }

    private static final Logger logger = Logger.getLogger(AttributeToGroupMapper.class);

    @Override
    public void updateBrokeredUser(KeycloakSession session, RealmModel realm, UserModel user, IdentityProviderMapperModel mapperModel, BrokeredIdentityContext context) {

        HashMap<String, GroupModel> userGroups = AddUserToGroup(session, realm, user, mapperModel, context, false);

        // leave from groups not present in SAML token
        for (GroupModel group: userGroups.values())
        {
            user.leaveGroup(group);
        }
    }


    public HashMap<String, GroupModel> AddUserToGroup(KeycloakSession session, RealmModel realm, UserModel user, IdentityProviderMapperModel mapperModel, BrokeredIdentityContext context, boolean isNewUser)
    {
        final String prefix = GetConfigValue(mapperModel, GROUP_PREFIX);
        final String name = GetConfigValue(mapperModel, ATTRIBUTE_NAME);
        final String friendly = GetConfigValue(mapperModel, ATTRIBUTE_FRIENDLY_NAME);

        // --------------------------------
        HashMap<String, GroupModel> userGroups = new HashMap<String, GroupModel>();

        if(!isNewUser)
        {
            user.getGroupsStream().forEach(gm->{
                if(prefix == null || gm.getName().startsWith(prefix))
                    userGroups.put(gm.getName(), gm);
            });
        }
        // --------------------------------

        AssertionType assertion = (AssertionType)context.getContextData().get(SAMLEndpoint.SAML_ASSERTION);

        for (AttributeStatementType statement : assertion.getAttributeStatements()) {
            for (AttributeStatementType.ASTChoiceType choice : statement.getAttributes()) {

                AttributeType attr = choice.getAttribute();

                if (name != null && !name.equals(attr.getName()))
                    continue;
                if (friendly != null && !friendly.equals(attr.getFriendlyName()))
                    continue;

                for (Object val : attr.getAttributeValue()) {
                    String groupName = val.toString();
                    if(prefix == null || groupName.startsWith(prefix))
                    {
                        GroupModel group = KeycloakModelUtils.findGroupByPath(realm, groupName);

                        if (group == null)
                        {
                            group = session.groups().createGroup(realm, groupName);
                        }
                        else if(!isNewUser && userGroups.containsKey(groupName))
                        {
                            userGroups.remove(groupName);
                            continue;
                        }

                        user.joinGroup(group);
                    }
                }
            }
        }

        return userGroups;
    }

    @Override
    public String getHelpText() {
        return "If a claim exists, grant the user the specified realm or application group.";
    }

    @Override
    public AttributeToGroupMapper create(KeycloakSession session) {
        return new AttributeToGroupMapper();
    }

    private String GetConfigValue(IdentityProviderMapperModel mapperModel, String key)
    {
        String value = mapperModel.getConfig().get(key);

        return value == null || value.trim().equals("")
                ? null
                : value.trim();
    }
}