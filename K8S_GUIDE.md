# K8S GUIDE

Step by step guide to install keycloak and friends in a GCP/GKE cluster.  
Assuming the cluster is already created, as well as a namespace.

1. create a secret (not a configmap because of the password):
```
kubectl -n qa create secret generic keycloak \
    --from-literal KC_DB_USERNAME="keycloak" \
    --from-literal KC_DB_PASSWORD='admin123' \
    --from-literal KC_DB_NAME="keycloak" \
    --from-literal KEYCLOAK_ADMIN='admin' \
    --from-literal KEYCLOAK_ADMIN_PASSWORD='admin123' \
```

2. create a disk:
```
gcloud compute disks create keycloak \
    --project=oecd-228113 \
    --type=pd-balanced \
    --size=10GB \
    --zone=europe-west1-b
```

3. apply the k8s strategy which includes:
- the service
- the statefulset
- the persistent volume and its claim

4. apply the k8s strategy which includes:
- the service
- the deployment
