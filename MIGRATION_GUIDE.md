# MIGRATION GUIDE
Step by guide to migration from Keycloak 12.0.4 to the latest Keycloak version (currently 24.x).

## disclaimer
Major changes to be aware of:
- Removal of WildFly-based keycloak (starting keycloak 17, now uses quarkus)
- New configuration format (keycloak.conf instead of standalone.xml)
- Database schema updates (automatic but irreversible)
- Changes in user federation and identity providers

## approaches
**Upgrade Keycloak In-Place**:  
Seems easier but requires to update Keycloak step-by-step (12 -> 13 -> ... -> 24) which will create too many variations and complexify SISCC support.  
Each step requires checks and potential config adaptation, as well as backups to avoid loosing data across steps and be forced to restart from the beginning.

**Export and Import Data**:  
Probably not the recommended approach because passwords won't be kept but given the gap between versions, the easiest to support.  
Also less disruptive as 2 keycloaks are required (the current and the new) which ensure no service disruption.  
At the end, the only downside is to force users to renew their passwords upon new connection to the new keycloak.  
An automatic mailing can be created to let users know they have to renew their password.

## migration
#### 1. export realm data (from UI)
- go to UI (old keycloak)
- go to export in your realm (not master)
- select the 2 options `Export groups and roles` and `Export clients`
- export to a JSON file

#### 2. import realm data (from UI)
- go to UI (new keycloak)
- create a realm from a resource and select the JSON file
- use the same name as the exported realm

#### 3. export users
access your keycloak server & run the following command to connect to the admin cli:
```
opt/jboss/keycloak/bin/kcadm.sh config credentials \
  --server http://localhost:8080/auth \
  --realm master \
  --user <ADMIN> \
  --password <ADMIN_PASSWORD>
```
_note: the admin user is in the master realm!_

once connected, export users into a JSON file:  
`opt/jboss/keycloak/bin/kcadm.sh get users -r <REALM> > /tmp/users.json -l 500`  
_note: 500 should be above the count of users in your keycloak._

#### 4. export groups
_user groups are not exported by keycloak!_

extract user ids in a txt file:  
`jq -r '.[].id' users.json > ids.txt`